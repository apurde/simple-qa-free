package de.edrup.confluence.plugins;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SimpleQADataServlet extends HttpServlet {
	
	private static final long serialVersionUID = 100L;
	private final SimpleQAHelper simpleQAHelper;
	
	
	public SimpleQADataServlet(SimpleQAHelper simpleQAHelper) {
		this.simpleQAHelper = simpleQAHelper;
	}
	
	
	// return the Simple QA data for the given page
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		Long pageId = Long.parseLong(request.getParameter("pageId"));
		String userKey = request.getParameter("userKey");
		
		// Confluence sometimes sends request with pageId = 0
		if(pageId == 0L) {
			response.sendError(400);
			return;
		}
		
		PrintWriter out = response.getWriter();
		out.print(simpleQAHelper.getSimpleQADataAndConfigForPage(pageId, userKey, false));
	}
	
	
	// set the Simple QA data for the given page
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		Long pageId = Long.parseLong(request.getParameter("pageId"));
		boolean notifyApproval = Boolean.parseBoolean(request.getParameter("notifyApproval"));
		//String data = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
		String data = convertStreamToString(request.getInputStream());
		if(!data.equals("")) {
			simpleQAHelper.setSimpleQADataForPage(pageId, data);
			
			if(notifyApproval) {
				simpleQAHelper.notifyApproval(pageId, data);
			}
		}
		
		PrintWriter out = response.getWriter();
		out.print(data);
	}
	
	
	// read the input stream input a string
	private String convertStreamToString(java.io.InputStream is) {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		try {
			while ((length = is.read(buffer)) != -1) {
			    result.write(buffer, 0, length);
			}
			return result.toString("UTF-8");
		}
		catch (Exception e) {
			return "";
		}
	}
}
