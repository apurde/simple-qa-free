package de.edrup.confluence.plugins;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SimpleQAOverviewServlet extends HttpServlet {
	
	private static final long serialVersionUID = 100L;
	private final SimpleQAHelper simpleQAHelper;
	
	
	public SimpleQAOverviewServlet(SimpleQAHelper simpleQAHelper) {
		this.simpleQAHelper = simpleQAHelper;
	}
	
	// return the requested overview of questions
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		Long pageId = Long.parseLong(request.getParameter("pageId"));
		String userKey = request.getParameter("userKey");
		String searchTerm = request.getParameter("searchTerm");
		String filter = request.getParameter("filter");
		
		// Confluence sometimes sends request with pageId = 0
		if(pageId == 0L) {
			response.sendError(400);
			return;
		}
		
		PrintWriter out = response.getWriter();
		out.print(simpleQAHelper.getSimpleQAOverview(pageId, userKey, searchTerm, filter));
	}
}
