package de.edrup.confluence.plugins;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;


public class SimpleQAOverview implements Macro {
	
	private static String justTableFor = "pdf word";
	private final SimpleQAHelper simpleQAHelper;
	
	
	public SimpleQAOverview(SimpleQAHelper simpleQAHelper) {
		this.simpleQAHelper = simpleQAHelper;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		
		// output to PDF and Word
		if(justTableFor.contains(conversionContext.getOutputType())) {
			ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
			return simpleQAHelper.getSimpleQAOverview(conversionContext.getEntity().getId(),
				((currentUser != null) ? currentUser.getKey().toString() : ""),
				"", "simple-qa-filter-all");
		}
		
		// screen output
		else {
			// set our context parameters
			context.put("treeViewPresent", simpleQAHelper.checkIfTreeViewActive());
			context.put("watchLink", simpleQAHelper.getWatchLink(conversionContext.getEntity()));
			context.put("unWatchLink", simpleQAHelper.getUnWatchLink(conversionContext.getEntity()));
			context.put("templateId", parameters.containsKey("templateId") ? parameters.get("templateId") : "default");
			context.put("contentBlueprintId", parameters.containsKey("contentBlueprintId") ? parameters.get("contentBlueprintId") : "default");
			
			// render legacy template
			if(GeneralUtil.getBuildNumber().compareTo("6441") < 0) {
				return VelocityUtils.getRenderedTemplate("/templates/simple-qa-overview-legacy.vm", context);			
			}
			
			// render current template
			else {
				return VelocityUtils.getRenderedTemplate("/templates/simple-qa-overview.vm", context);
			}
		}
	}
	
	
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}
	

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
