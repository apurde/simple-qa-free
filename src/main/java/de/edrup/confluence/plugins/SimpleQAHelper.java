package de.edrup.confluence.plugins;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.query.TextQuery;
import com.atlassian.confluence.search.v2.searchfilter.InSpaceSearchFilter;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.json.jsonorg.JSONArray;
import com.atlassian.json.jsonorg.JSONObject;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;
import static com.google.common.collect.Lists.partition;

@Named
public class SimpleQAHelper {
	
	private final PageManager pageMan;
	private final ContentPropertyManager propMan;
	private final LikeManager likeMan;
	private final SettingsManager settingsMan;
	private final NotificationManager notificationMan;
	private final UserAccessor userAcc;
	private final SearchManager searchMan;
	private final PermissionManager permissionMan;
	private final CommentManager commentMan;
	private final XhtmlContent xhtmlUtils;
	private final SpaceManager spaceMan;
	private final I18nResolver i18n;
	private final TransactionTemplate transactionTemplate;
	private final MultiQueueTaskManager mqtm;
	private final PluginAccessor pluginAcc;

	private static final Logger log = LoggerFactory.getLogger(SimpleQAHelper.class);
	
	private static final int THREAD_POOL_SIZE = Integer.getInteger("confluence.masterdetails.thread.pool.size", 4);
    private static ExecutorService pool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    
    private static final long RENDER_TIMEOUT_SECONDS = 5L;
    private static final int RENDER_SUBSET_SIZE = 10;

	
	@Inject
	public SimpleQAHelper(@ComponentImport PageManager pageMan, @ComponentImport SettingsManager settingsMan,
		@ComponentImport ContentPropertyManager propMan, @ComponentImport LikeManager likeMan,
		@ComponentImport NotificationManager notificationMan, @ComponentImport UserAccessor userAcc,
		@ComponentImport SearchManager searchMan, @ComponentImport PermissionManager permissionMan,
		@ComponentImport CommentManager commentMan, @ComponentImport XhtmlContent xhtmlUtils,
		@ComponentImport SpaceManager spaceMan, @ComponentImport I18nResolver i18n,
		@ComponentImport TransactionTemplate transactionTemplate, @ComponentImport MultiQueueTaskManager mqtm,
		@ComponentImport PluginAccessor pluginAcc) {
		
		this.pageMan = pageMan;
		this.settingsMan = settingsMan;
		this.propMan = propMan;
		this.likeMan = likeMan;
		this.notificationMan = notificationMan;
		this.userAcc = userAcc;
		this.searchMan = searchMan;
		this.permissionMan = permissionMan;
		this.commentMan = commentMan;
		this.xhtmlUtils = xhtmlUtils;
		this.spaceMan = spaceMan;
		this.i18n = i18n;		
		this.transactionTemplate = transactionTemplate;
		this.mqtm = mqtm;
		this.pluginAcc = pluginAcc;
	}
	
		
	// get the Simple QA data and configuration data for the given page id
	public String getSimpleQADataAndConfigForPage(Long id, String userKey, boolean overviewMode) {
		
		// the default
		String simpleQAData = "{\"active\":false}";
		
		// the initial value for new questions
		String simpleQADataActiveEmpty = "{\"active\":true,\"ownerId\":\"$owner\",\"views\":0,\"approvedWithCommentId\":\"\",\"votes\":[]}";
				
		try {
			// is the page a descendant of a page containing our simple-qa-overview macro?
			if(pageMan.getPage(id).getParent() != null) {
				if(pageMan.getPage(id).getParent().getBodyAsString().contains("simple-qa-overview")) {
					
					// get the data
					simpleQAData = propMan.getTextProperty(pageMan.getPage(id), "simple-qa-data");
									
					// in case we didn't find the data
					if(simpleQAData == null) {	
						simpleQAData = simpleQADataActiveEmpty.replace("$owner", pageMan.getPage(id).getCreator().getKey().toString());
					}
				}
			}
		}
		catch(Exception e) {
			log.error("Error getting data and configuration: {}", e.toString());
			log.debug(getStackTrace(e));
		}
		
		//TODO: remove this line in the next version
		simpleQAData = simpleQAData.replace("answeredWithCommentId", "approvedWithCommentId");
		
		// convert the data to a JSON object
		JSONObject simpleQADataJSON = new JSONObject(simpleQAData);
				
		// get the configuration data of the Simple QA Overview
		JSONObject configDataJSON = overviewMode ? new JSONObject() : getOverviewConfigData(pageMan.getPage(id).getParent(), userKey);
		
		// combine the two results
		JSONObject simpleQAContainer = new JSONObject();
		simpleQAContainer.put("data", simpleQADataJSON);
		simpleQAContainer.put("config", configDataJSON);
		
		// return that Simple QA is not active on that page
		return simpleQAContainer.toString();
	}

	
	// set the Simple QA data for the given page id
	public void setSimpleQADataForPage(Long id, String simpleQAData) {
		propMan.setTextProperty(pageMan.getPage(id), "simple-qa-data", simpleQAData);
		log.debug("Setting Simpla QA data for page {}: {}", id.toString(), simpleQAData);
	}
	
	
	// get the question overview
	public String getSimpleQAOverview(Long pageId, String userKey, String searchTerm, String filter) {
		
		// debug
		Date startTime = new Date();
		
		// get the configuration
		JSONObject overviewConfigData = getOverviewConfigData(pageMan.getPage(pageId), userKey);
		
		// the array to store our results in
		ArrayList<JSONObject> entries = new ArrayList<JSONObject>();
		
		// get all descendants of the given page
		Collection<Long> descIDs = pageMan.getDescendantIds(pageMan.getPage(pageId));
		
		// prepare a container for our results
		ArrayList<Long> results = new ArrayList<Long>();
		
		// perform search in case a term is defined
		if(searchTerm.length() > 0) {
			
			// prepare the search
			SearchQuery query = new TextQuery(searchTerm);
			HashSet<String> keys = new HashSet<String>();
			keys.add(pageMan.getPage(pageId).getSpaceKey());
			InSpaceSearchFilter spaceFilter = new InSpaceSearchFilter(keys);
			ContentSearch contentSearch = new ContentSearch(query, null, spaceFilter, 0, 1500);
			
			// try to perform the search
			try {
				SearchResults searchResult = searchMan.search(contentSearch);
				List<SearchResult> searchResultList = searchResult.getAll();
				
				log.debug("Search for {} returned {} hits in the space.", searchTerm, searchResultList.size());
				
				// loop through all results
				for(int n = 0; n < searchResultList.size(); n++) {
					
					String pageTitle = "";
					switch(searchResultList.get(n).getType().toLowerCase()) {
						case "page": pageTitle = searchResultList.get(n).getDisplayTitle(); break;
						case "comment": pageTitle = searchResultList.get(n).getExtraFields().get("containingPageDisplayTitle"); break;
						default: break;
					}
					
					log.debug("Single search result: type: {}; determined page title: {}", searchResultList.get(n).getType().toLowerCase(), pageTitle);
					
					if(!pageTitle.equals("")) {
					
						// locate the page matching the result
						Page p =  pageMan.getPage(searchResultList.get(n).getSpaceKey(), pageTitle);
						
						if(p != null) {
							// in case it is a descendant and we don't have the result
							if(descIDs.contains(p.getId()) && !results.contains(p.getId())) {
								// add it to our result list
								results.add(p.getId());
							}
							else {
								log.debug("Skipping page {} as it is not a descendant or we already have it.", p.getTitle());
							}
						}
						
						// unable to find the page to the result
						else {
							log.error("Could not find page for result: {}, {}", searchResultList.get(n).getSpaceKey(), pageTitle);
						}
					}
				}
			}
			
			// in case something fails we simply show all descendants as result
			// remark: in issue #5 it has been reported that descIDs might contain duplicates; to avoid this we don't use addAll here
			catch (Exception e) {
				results.clear();
				for(Long id : descIDs) {
					if(!results.contains(id)) {
						results.add(id);
					}
				}
				log.error("Search returned an error: {}", e.toString());
				log.debug(getStackTrace(e));
			}
		}
		
		// take all descendants in case no search term is defined
		// remark: in issue #5 it has been reported that descIDs might contain duplicates; to avoid this we don't use addAll here
		else {
			for(Long id : descIDs) {
				if(!results.contains(id)) {
					results.add(id);
				}
			}
		}
		
		// debug
		Date nowTime = new Date();
		log.debug("Collecting the relevant pages finished at {} ms.", nowTime.getTime() - startTime.getTime());
		log.debug("Found {} relevant pages.", results.size());
		
		// render subsets async
        final List<CompletableFuture<ArrayList<JSONObject>>> futures = new ArrayList<>();
        final ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
        partition(results, RENDER_SUBSET_SIZE).forEach(subset ->
            futures.add(CompletableFuture.supplyAsync(() -> renderSubsetInTransaction(currentUser, subset, userKey, filter, overviewConfigData), pool))
        );
        
        // debug
        log.debug("Rendering with {} subsets.", futures.size());
        
        // get all results back
        for (CompletableFuture<ArrayList<JSONObject>> future : futures) {
            try {
            	ArrayList<JSONObject> futureResult = future.get(RENDER_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                entries.addAll(futureResult);
            }
            catch(Exception e) {
                log.error("Putting together the results returned this error: {}", e.toString());
                log.debug(getStackTrace(e));
            }
        };
        
        // debug
        log.debug("After rendering {} pages left.", entries.size());

		// sort the array (newest first)
		Collections.sort(entries, new Comparator<JSONObject>(){
			@Override
	        public int compare(JSONObject a, JSONObject b) {
				return b.getString("last").compareTo(a.getString("last"));
	        }
		});	
		
		// use velocity to render the result
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("entries", entries);
		context.put("numEntries", entries.size());
		String renderedTemplate = VelocityUtils.getRenderedTemplate("/templates/simple-qa-table.vm", context);
		
		// debug
		nowTime = new Date();
		log.debug("Getting the rendered overview finished at {} ms.", nowTime.getTime() - startTime.getTime());
		
		return renderedTemplate;
	}
	
	
	// render the given subset of the overview table in a transaction template
	private ArrayList<JSONObject> renderSubsetInTransaction(ConfluenceUser user, List<Long> pageIDs, String userKey, String filter, JSONObject overviewConfigData) {
		try {
            AuthenticatedUserThreadLocal.set(user);
            return transactionTemplate.execute(() -> renderSubset(pageIDs, userKey, filter, overviewConfigData));
        } catch (Exception e) {
            log.error("Rendering in subset returned this error: {}", e.toString());
            log.debug(getStackTrace(e));
            return new ArrayList<JSONObject>();
        }
	}
	
	
	// render the given subset of the overview table
	private ArrayList<JSONObject> renderSubset(List<Long> pageIDs, String userKey, String filter, JSONObject overviewConfigData) {

		// debug
		Date startTime = new Date();
		
		// the initially empty subset
		ArrayList<JSONObject> subset = new ArrayList<JSONObject>();
		
		// loop through the question pages of our subset
		for(Long pageID : pageIDs) {
			try {
				// get the data we need
				String simpleQAContainer = getSimpleQADataAndConfigForPage(pageID, userKey, true);
				JSONObject simpleQAData = new JSONObject(simpleQAContainer).getJSONObject("data");
				Page p = pageMan.getPage(pageID);
								
				// in case it passes the filter
				if(passesFilter(filter, simpleQAData, userKey, p)) {
					
					// populate a new entry
					JSONObject entry = new JSONObject();
					entry.put("views", simpleQAData.getInt("views"));
					entry.put("answers", getAnswerCount(simpleQAData.getJSONArray("votes")));
					entry.put("titleAndLink", String.format("<a href='%s'>%s</a>", settingsMan.getGlobalSettings().getBaseUrl() + p.getUrlPath(), StringEscapeUtils.escapeHtml4(p.getTitle())));
					ConfluenceUser u = userAcc.getUserByKey(new UserKey(simpleQAData.getString("ownerId")));
					entry.put("userName", ((u != null) ? u.getFullName() : "?"));
					entry.put("bodyExcerpt", getBodyExcerpt(p, overviewConfigData));
					entry.put("answered", !simpleQAData.getString("approvedWithCommentId").equals(""));
					entry.put("last", getLastActivity(p));
					entry.put("likes", likeMan.getLikes(p).size());
					entry.put("lastModifier", p.getLastModifier().getFullName());
					
					JSONArray labels = new JSONArray();
					for(Label label : p.getLabels()) {
						JSONObject labelText = new JSONObject();
						labelText.put("text", label.getName());
						labels.put(labelText);
					}
					entry.put("labels", labels);
					entry.put("labelCount", labels.length());
					
					// add the entry to our list
					subset.add(entry);
				}
			}
			catch(Exception e) {
				log.error("Render in subset returned this error: {}", e.toString());
				log.debug(getStackTrace(e));
			}
		}
		
		// debug
		Date nowTime = new Date();
		log.debug("Preparing the subset took {} ms.", nowTime.getTime() - startTime.getTime());
		
		return subset;
	}
	
	
	// get the answers in HTML for export
	public String getAnswersForPage(Long pageId, ConversionContext conversionContext) {
		
		String simpleQAContainer = getSimpleQADataAndConfigForPage(pageId, "", true);
		JSONObject simpleQAData = new JSONObject(simpleQAContainer).getJSONObject("data");

		// build an array with the necessary data for the Velocity template
		ArrayList<JSONObject> entries = new ArrayList<JSONObject>();
		for(int n = 0; n < simpleQAData.getJSONArray("votes").length(); n++) {
			try {
				JSONObject entry = new JSONObject();
				entry.put("votes", simpleQAData.getJSONArray("votes").getJSONObject(n).getInt("count"));
				entry.put("answered", simpleQAData.getString("approvedWithCommentId").equals(simpleQAData.getJSONArray("votes").getJSONObject(n).getString("id")));
				Long commentId = Long.parseLong(simpleQAData.getJSONArray("votes").getJSONObject(n).getString("id"));
				entry.put("owner", commentMan.getComment(commentId).getCreator().getFullName());
				String contentInHTML = xhtmlUtils.convertStorageToView(commentMan.getComment(commentId).getBodyAsString(), conversionContext);
				entry.put("content", contentInHTML);
				entry.put("isComment", simpleQAData.getJSONArray("votes").getJSONObject(n).has("isComment") ? simpleQAData.getJSONArray("votes").getJSONObject(n).getBoolean("isComment") : false);
				entry.put("level", 0);
				entries.add(entry);
				entries.addAll(getAllChilds(commentId, 1, simpleQAData.getJSONArray("votes").getJSONObject(n).getInt("count"), conversionContext));
			}
			catch(Exception e) {
				log.error("Getting answers for export returned this error: {}", e.toString());
				log.debug(getStackTrace(e));
			}
		}
		
		// sort the array (most votes first)
		Collections.sort(entries, new Comparator<JSONObject>(){
			@Override
	        public int compare(JSONObject a, JSONObject b) {
				return (a.getInt("votes") <= b.getInt("votes")) ? 1 : -1;
	        }
		});
		
		// use velocity to render the result
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("entries", entries);
		context.put("numEntries", entries.size());
		return VelocityUtils.getRenderedTemplate("/templates/simple-qa-answers.vm", context);
	}
	
	
	// get all comments below the given comment
	private ArrayList<JSONObject> getAllChilds(Long id, int level, int votes, ConversionContext conversionContext) {
	
		// get all children of the given comment
		ArrayList<Comment> childs = new ArrayList<Comment>();
		childs.addAll(commentMan.getComment(id).getChildren());
		
		// prepare a list of child data for the export
		ArrayList<JSONObject> childData = new ArrayList<JSONObject>();
		
		// add all children
		for(int n = 0; n < childs.size(); n++) {
			
			// add the child itself
			childData.add(dataForChildComment(childs.get(n).getId(), level, votes, conversionContext));
			
			// and recursively all grand-children
			childData.addAll(getAllChilds(childs.get(n).getId(), level + 1, votes, conversionContext));
		}
		
		return childData;
	}
	
	
	// add the data for the specified child
	private JSONObject dataForChildComment(Long id, int level, int votes, ConversionContext conversionContext) {
		
		JSONObject entry = new JSONObject();
		entry.put("isComment", true);
		entry.put("level", level);
		entry.put("owner", commentMan.getById(id).getCreator().getFullName());
		
		String contentInHTML = "";
		try {
			contentInHTML = xhtmlUtils.convertStorageToView(commentMan.getComment(id).getBodyAsString(), conversionContext);
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
		entry.put("content", contentInHTML);
		
		entry.put("votes", votes);
		entry.put("answered", false);
		
		return entry;
	}
	
	
	// check if a result passes the filter
	private boolean passesFilter(String filter, JSONObject simpleQAData, String userKey, Page p) {
		try {
			
			// check permission
			if(!permissionMan.hasPermission(userAcc.getUserByKey(new UserKey(userKey)), Permission.VIEW, p)) {
				return false;
			}
			
			// check for active
			if(!simpleQAData.getBoolean("active")) {
				return false;
			}
			
			switch(filter) {
				case "simple-qa-filter-all" : return true;
				case "simple-qa-filter-answered" : return (getAnswerCount(simpleQAData.getJSONArray("votes")) > 0);
				case "simple-qa-filter-unanswered" : return (getAnswerCount(simpleQAData.getJSONArray("votes")) == 0);
				case "simple-qa-filter-mine" :
					if(userKey.equals("")) {
						return false;
					}
					else {
						return (simpleQAData.getString("ownerId").equals(userKey));
					}
				case "simple-qa-filter-watch" :
					if(userKey.equals("")) {
						return false;
					}
					else {
						return notificationMan.isWatchingContent(userAcc.getUserByKey(new UserKey(userKey)), p);
					}
				case "simple-qa-filter-notaccepted" :
					return simpleQAData.getString("approvedWithCommentId").isEmpty();
				default : return true;
			}
		}
		catch(Exception e) {
			log.error("Passes filter returned this error: {}", e.toString());
			log.debug(getStackTrace(e));
			return true;
		}
	}
	
	
	// return an excerpt of the body
	private String getBodyExcerpt(Page p, JSONObject overviewConfigData) {
		
		// get the body
		String content = p.getBodyAsStringWithoutMarkup();
		
		// extract from the beginning or after the given string
		int startIndex = 0;
		if(overviewConfigData.has("startExtractAfter") && !overviewConfigData.getString("startExtractAfter").isEmpty()) {
			startIndex = content.indexOf(overviewConfigData.getString("startExtractAfter"));
			if(startIndex > -1) {
				startIndex = startIndex + overviewConfigData.getString("startExtractAfter").length();
			}
			else {
				startIndex = 0;
			}
		}
		
		// return the extract
		if((content.length() - startIndex) > 200) {
			return content.substring(startIndex, 200) + "...";
		}
		else {
			return content.substring(startIndex);
		}
	}
	
	
	// return the last activity date of a question
	private String getLastActivity(Page p) {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		// the last activity of the page
		Date lastActivity = p.getLastModificationDate();
		
		// check whether a comment was later
		List<Comment> answers = p.getComments();
		for(int c = 0; c < answers.size(); c++) {
			if(answers.get(c).getLastModificationDate().after(lastActivity)) {
				lastActivity = answers.get(c).getLastModificationDate();
			}
		}
		
		// return the last activity
		return df.format(lastActivity);
	}
	
	
	// return the number of answers
	private int getAnswerCount(JSONArray votes) {
		int count = 0;
		for(int n = 0; n < votes.length(); n++) {
			if(votes.getJSONObject(n).has("isComment")) {
				if(!votes.getJSONObject(n).getBoolean("isComment")) {
					count = count + 1;
				}
			}
			else {
				count = count + 1;
			}
		}
		
		return count;
	}
	
	
	// get the detailed stack trace
	private static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
	
	
	// create the Simple QA additional configuration data
	private JSONObject getOverviewConfigData(Page p, String userKey) {
		
		JSONObject configData = new JSONObject();
		
		if(p != null) {
			
			HashMap<String, String> overviewParams = getOverviewParams(p);
			ConfluenceUser confUser = userKey.equals("") ? null : userAcc.getUserByKey(new UserKey(userKey));
			
			// the "easy" parameters
			configData.put("allowDownVote", overviewParams.containsKey("allowDownVote") ? overviewParams.get("allowDownVote").equals("true") : true);
			configData.put("showInstructions", overviewParams.containsKey("showInstructions") ? overviewParams.get("showInstructions").equals("true") : true);
			configData.put("startExtractAfter", overviewParams.containsKey("startExtractAfter") ? overviewParams.get("startExtractAfter") : "");
			
			// the instructional message
			String messageTitle = overviewParams.containsKey("instructionsTitle") ? overviewParams.get("instructionsTitle") : "";
			String messageBody = overviewParams.containsKey("instructionsBody") ? overviewParams.get("instructionsBody") : "";
			configData.put("instructionsTitle", messageTitle.equals("") ? i18n.getText("de.edrup.confluence.plugins.simple-qa.simple-qa-overview.param.instructionsTitle.default") : messageTitle);
			configData.put("instructionsBody", messageBody.equals("") ? i18n.getText("de.edrup.confluence.plugins.simple-qa.simple-qa-overview.param.instructionsBody.default") : messageBody);
		
			// is the current user allowed to approve additionally to the owner?
			boolean approveAllowed = false;
			try {
				String approveUsers = overviewParams.containsKey("approveUsers") ? overviewParams.get("approveUsers") : "";
				if(confUser != null) {
					if(approveUsers.contains(confUser.getName())) {
						boolean lastChangeBySpaceOrConfAdmin = spaceMan.getSpaceAdmins(p.getSpace()).contains(p.getLastModifier()) | permissionMan.isConfluenceAdministrator(p.getLastModifier());
						if(p.getCreator().equals(p.getLastModifier()) || lastChangeBySpaceOrConfAdmin) {
							approveAllowed = true;
						}
						else {
							log.info("User allowed to accept an answer was detected but the last change of the page was not made by the creator or an admin.");
						}
					}
				}
			}
			catch(Exception e) {
				log.error("Get overview config data returned this error: {}", e.toString());
				log.debug(getStackTrace(e));
			}		
			configData.put("approveAllowed", approveAllowed);
		}

		return configData;
	}
	
	
	// get the params of the Simple QA Overview Macro on page p
	private HashMap<String, String> getOverviewParams(Page p) {
		
		HashMap<String, String> params = new HashMap<String, String>();
		
		try
		{
			xhtmlUtils.handleMacroDefinitions(p.getBodyAsString(), new DefaultConversionContext(p.toPageContext()), new MacroDefinitionHandler()
			{
				@Override
				public void handle(MacroDefinition macroDefinition) {
					if(macroDefinition.getName().equals("simple-qa-overview")) {
						params.putAll(macroDefinition.getParameters());
					}
				}
			});
		}
	    catch (Exception e) {
	    	log.error("Get overview params returned this error: {}", e.toString());
	    	log.debug(getStackTrace(e));
	    }
		
		return params;
	}
	
	
	// notify the owner of an answer when it is approved
	public void notifyApproval(Long pageId, String data) {
		try {
			// determine the data we need
			JSONObject simpleQAData = new JSONObject(data);
			ConfluenceUser u = commentMan.getComment(Long.parseLong(simpleQAData.getString("approvedWithCommentId"))).getCreator();
			Page p = pageMan.getPage(pageId);
			
			// in case he/she is watching the question
			if(notificationMan.isWatchingContent(u, p)) {
				
				// generate a new message
				MailQueueItem mqi = new ConfluenceMailQueueItem(u.getEmail(),
					i18n.getText("de.edrup.confluence.plugins.simple-qa.notification.title"),
					i18n.getText("de.edrup.confluence.plugins.simple-qa.notification.body").
						replace("$questionWithLink",String.format("<a href='%s'>%s</a>", settingsMan.getGlobalSettings().getBaseUrl() + p.getUrlPath(), StringEscapeUtils.escapeHtml4(p.getTitle()))),
					MIME_TYPE_HTML);
				
				log.debug("Sending email to {} that his/her answer to {} has been accepted.", u.getFullName(), p.getTitle());
				
				// add the new message as task to the queue
				mqtm.addTask("mail", mqi);		

			}
		}
		catch(Exception e) {
	    	log.error("Notifying the answerer returned this error: {}", e.toString());
	    	log.debug(getStackTrace(e));			
		}
	}
	
	
	// check if tree-view is installed and enabled
	public boolean checkIfTreeViewActive() {
		try {
			return pluginAcc.isPluginEnabled("de.edrup.confluence.plugins.tree-view");
		}
		catch(Exception e) {
			log.error("Checking for Tree View returned this error: {}", e.toString());
			log.debug(getStackTrace(e));
		}
		return false;
	}
	
	
	// get the tree-view link for watching
	public String getWatchLink(ContentEntityObject ceo) {
		return settingsMan.getGlobalSettings().getBaseUrl() + "/pages/watch-tree.action?pageId=" + Long.toString(ceo.getId());
	}
	
	
	// get the tree-view link for unwatching
	public String getUnWatchLink(ContentEntityObject ceo) {
		return settingsMan.getGlobalSettings().getBaseUrl() + "/pages/unwatch-tree.action?pageId=" + Long.toString(ceo.getId());
	}
}
