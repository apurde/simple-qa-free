package de.edrup.confluence.plugins;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

public class SimpleQAExportView implements Macro {
	
	private static String outputFor = "pdf word";
	private final SimpleQAHelper simpleQAHelper;
	
	
	public SimpleQAExportView(SimpleQAHelper simpleQAHelper) {
		this.simpleQAHelper = simpleQAHelper;
	}


	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
		ConversionContext conversionContext) throws MacroExecutionException {
		
		if(outputFor.contains(conversionContext.getOutputType())) {
			return simpleQAHelper.getAnswersForPage(conversionContext.getEntity().getId(), conversionContext);
		}
		else {
			return "";
		}
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
