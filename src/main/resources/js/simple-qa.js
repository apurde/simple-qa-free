// "class" to handle all Simple QA functions on a question page
function SimpleQA() {

	// default data
	var simpleQAData = {
		"active" : false,
		"ownerId" : "",
		"views" : 0,
		"approvedWithCommentId": "",
		"votes" : []
	};
	
	var simpleQAConfig = {
		"allowADownVote" : true,
		"showInstructions" : true,
		"approveAllowed" : false,
		"instructionsTitle" : "",
		"instructionsBody" : ""
	};
	
	var messageFlag = null;
	var buttonsBlocked = false;
	var currentUserId = "";
	var mobile = false;
	var pageId = "";
	var baseUrl = "";
	var jz = null;
	var lastUpdate = 0;
	
	
	// public functions
	this.init = function(mobileMode, jqueryOrZepto) {
		
		mobile = mobileMode;
		jz = jqueryOrZepto;
		
		if(!mobile) {
			currentUserId = AJS.Meta.get("remote-user-key");
			baseUrl = AJS.contextPath();
			pageId =  AJS.Meta.get("page-id");
			
			if(AJS.Meta.get("content-type") == "page") {
				getSimpleQAData(afterInitialDataReceived);
			}
		}
		else {
			var container = document.getElementById("simple-qa-mobile-container");
			if(container) {
				pageId = jz(container).attr("pageId");
				currentUserId = jz(container).attr("userKey");
				baseUrl = jz(container).attr("baseUrl");
				getSimpleQAData(afterInitialDataReceivedMobile);
			}
		}
	};

	this.upVote = function(id) {
		voteChange(id, true);
	};
	
	this.downVote = function(id) {
		voteChange(id, false);
	};
	
	this.approveAnswer = function(id) {
		approveAnswerInt(id);
	};
	
	this.optOut = function() {
		optOutInt();
	}
	
	this.convertToComment = function(id) {
		convertToCommentInt(id);
	}
	
	
	// function called when settings have been received from server
	var afterInitialDataReceived = function() {
		if(simpleQAData.active) {
			applyDecorators();
			applyStatusStyles();
			sortAnswers();
			startObserver();
			checkForOrphanedData();
			countView();
			showMessage();
		}
	}
	
	
	// function called when settings have been received from server (mobile)
	var afterInitialDataReceivedMobile = function() {
		if(simpleQAData.active) {
			applyDecoratorsMobile();
			startObserver();
			countView();
		}
	}

	
	// apply the control elements to each answer
	var applyDecorators = function() {
	
		var simpleQAControlsEmpty = "<div class='simple-qa-div' style='text-align:center;float:left' id='simple-qa-div-commentId'><a id='simple-qa-upvote-commentId' class='simple-qa-action' href='javascript:simpleQA.upVote(commentId);' title='$upVoteTooltip'><span class='aui-icon aui-icon-large aui-icon-large-to-24px aui-iconfont-up'></span></a><br/>" + 
			"<span class='simple-qa-vote-count' id='simple-qa-vote-count-commentId' style='font-size:16px'>$votes</span><br/>" +
			"<a id='simple-qa-downvote-commentId' class='simple-qa-downvote simple-qa-action' href='javascript:simpleQA.downVote(commentId);' title='$downVoteTooltip'><span class='aui-icon aui-icon-large aui-icon-large-to-24px aui-iconfont-down'></span></a><br class='simple-qa-downvote-br'/>" +
			"$approveDecorator" +
			"<span id='simple-qa-comment-commentId' style='display:none;' class='aui-icon aui-icon-large aui-icon-large-to-20px simple-qa-comment aui-iconfont-comment' title='$cTooltip'></span></div>";
		var approveDecoratorActive = "<a id='simple-qa-approve-commentId' class='simple-qa-action' href='javascript:simpleQA.approveAnswer(commentId);' title='$approveTooltip'><span class='aui-icon aui-icon-large aui-icon-large-to-20px aui-iconfont-approve'></span></a>";		
		var approveDecoratorInactive = "<span id='simple-qa-approve-commentId' class='aui-icon aui-icon-large aui-icon-large-to-20px aui-iconfont-approve'></span>";
		var convertToComment = "<li id='simple-qa-convert-commentId'><a href='javascript:simpleQA.convertToComment(commentId);'>" + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.convert") + "</a></li>";
		
		// find all comments
		var comments = jz("#comments-section").find(".comment").not(".add");
		
		// apply the decorators
		for(var c = 0; c < comments.length; c++) {
			var comment = comments[c];
		
			// only apply to comments on the top-level not to comments on comments; also don't modify anything in case it already has been done
			if(jz(comment).parent().parent().hasClass("top-level") && (jz(comment).find(".simple-qa-action").length == 0)) {
				
				// our commentId
				var commentId = extractCommentId(jz(comment).parent().attr('id'));
							
				// take the right approveDecorator or nothing
				var approveDecorator = ((simpleQAData.ownerId == currentUserId) || simpleQAConfig.approveAllowed) ? approveDecoratorActive : ((commentId == simpleQAData.approvedWithCommentId) ? approveDecoratorInactive : "");
				
				// fill empty decorator with life
				var simpleQAControls = simpleQAControlsEmpty.replace("$approveDecorator", approveDecorator).
					replace(/commentId/g, commentId).replace("$votes", getVoteCount(commentId)).
					replace("$upVoteTooltip", AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.upVote.tooltip")).
					replace("$downVoteTooltip", AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.downVote.tooltip")).
					replace("$approveTooltip", AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.approve.tooltip")).
					replace("$cTooltip", AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.comment.tooltip"));

				// insert decorator at the beginning of the comment
				jz(comment).prepend(simpleQAControls);
				
				// change alignment of other elements in the comment
				jz(comment).find(".comment-user-logo").css("padding-left", "4px");
				jz(comment).find(".comment-header").css("padding-left", "70px");
				jz(comment).find(".comment-body").css("padding-left", "70px");
				jz(comment).find(".comment-actions").css("clear", "none");
				
				// apply changes to the parent of the comment
				jz(comment).parent().addClass("simple-qa-answer");
				jz(comment).parent().prop("simple-qa-count",getVoteCount(extractCommentId(jz(comment).parent().attr('id'))));
				jz(comment).parent().prop("simple-qa-commentId", commentId);
				
				// apply border to accepted answer
				if(commentId == simpleQAData.approvedWithCommentId) {
					jz("#simple-qa-div-" + commentId).addClass("simple-qa-approved-div");
				}
				
				// in case an answer is rated as comment
				if(isComment(commentId)) {
					displayAsComment(commentId);
				}
				
				// in case it is not a comment we might need to apply the conversion link
				if(!isComment(commentId) && (commentId != simpleQAData.approvedWithCommentId)) {
					if(AJS.Meta.get("remote-user") == jz(comment).find(".confluence-userlink").first().attr("data-username")) {
						jz(comment).find(".comment-actions-primary").append(convertToComment.replace(/commentId/g, commentId));
					}
				}
			}
		}
		
		// remove the down-vote decorator again if not wanted
		if(!simpleQAConfig.allowDownVote) {
			jz(".simple-qa-downvote").hide();
			jz(".simple-qa-downvote-br").hide();
		}
		
		// replace text which indicates comments rather than answers
		commentsToAnswers();
	};
	
	
	// apply the read only decorators on mobile
	var applyDecoratorsMobile = function() {

		// find all comments
		var comments = jz("li[id^='comment-']");
		
		// loop through all comments
		for(var c = 0; c < comments.length; c++) {
			
			var comment = comments[c];
			
			if(jz(comment).parent().hasClass('top-level')) {
				
				var commentId = extractCommentId(jz(comment).attr("id"));
				var count = getVoteCount(commentId);
				var approved = (commentId == simpleQAData.approvedWithCommentId);
				var heading = jz(comment).find(".comment-author-name").first();
				var simpleQADecoratorPresent = (jz(heading).html().indexOf("simple-qa-mobile-span") > -1);
				
				if(approved && !simpleQADecoratorPresent && !isComment(commentId)) {
					jz(heading).html(jz(heading).html() + " <span class='simple-qa-mobile-span' style='color:green'>(" + count + " " + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.mobile.votes") + " &#x2714;&#xfe0e;)</span>");
				}
				
				if(!approved && !simpleQADecoratorPresent && !isComment(commentId)) {
					jz(heading).html(jz(heading).html() + " <span class='simple-qa-mobile-span'>(" + count + " " + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.mobile.votes") + ")</span>");
				}
			}
		}
			
		// replace text which indicates comments rather than answers
		commentsToAnswersMobile();
	};
	
	
	// extract the Id out of the provided id
	var extractCommentId = function(id) {
		return id.replace("comment-", "").replace("thread-", "");
	};
	
	
	// get the vote count for an answer
	var getVoteCount = function(id) {
	
		// search for all IDs we have
		for(var n = 0; n < simpleQAData.votes.length; n++) {
			if(simpleQAData.votes[n].id == id) {	
				return simpleQAData.votes[n].count;
			}
		}
		
		// id does not existing in our list
		console.log("Adding new answer vote for id: " + id);
		var newVote = {
			"id" : id,
			"count" : 0,
			"users" : "",
			"isComment" : false
		};
		simpleQAData.votes.push(newVote);
		postSimpleQAData(false);
		
		return "0";
	};
	
	
	// check if answer should be rated as comment
	var isComment = function(id) {
		
		// search for all IDs we have
		for(var n = 0; n < simpleQAData.votes.length; n++) {
			if(simpleQAData.votes[n].id == id) {
				return (simpleQAData.votes[n].isComment != null) ? simpleQAData.votes[n].isComment : false;
			}
		}
		
		return false;
	}
	
	
	// display an answer as a comment
	var displayAsComment = function(id) {
		jz("#simple-qa-upvote-" + id).hide();
		jz("#simple-qa-vote-count-" + id).hide();
		jz("#simple-qa-downvote-" + id).hide();
		jz("#simple-qa-div-" + id).find("br").hide();
		jz("#simple-qa-approve-" + id).hide();
		jz("#simple-qa-convert-" + id).hide();
		jz("#simple-qa-comment-" + id).show();
	}
	
	
	// apply the status styles to the controls
	var applyStatusStyles = function() {
	
		// answered sign
		if(simpleQAData.approvedWithCommentId != "") {
			jz("#simple-qa-approve-" + simpleQAData.approvedWithCommentId).addClass("simple-qa-approved");
		}
		
		// vote status for the active user
		for(var v = 0; v < simpleQAData.votes.length; v++) {
			var pos = simpleQAData.votes[v].users.indexOf(currentUserId);
			if((pos > -1) && (currentUserId.length > 0)) {
				if(charAfterString(simpleQAData.votes[v].users, currentUserId) == "+") {
					jz("#simple-qa-upvote-" + simpleQAData.votes[v].id).addClass("simple-qa-active-vote");
				}
				else {
					jz("#simple-qa-downvote-" + simpleQAData.votes[v].id).addClass("simple-qa-active-vote");
				}
			}
		}
	};
	
	
	// vote up an answer
	var voteChange = function(id, up) {
	
		// exist in case of anonymous user
		if(currentUserId == "") return;
		
		// if buttons are still blocked return - otherwise block them
		if(buttonsBlocked) return;
		blockButtons();
	
		// update data before continuing
		getSimpleQAData(function() {
			
			// look for the given id in the votes
			for(var v = 0; v < simpleQAData.votes.length; v++) {
				if(simpleQAData.votes[v].id == id) {
					
					// remove active votes visually (we set them again later if needed)
					jz("#simple-qa-upvote-" + id).removeClass("simple-qa-active-vote");
					jz("#simple-qa-downvote-" + id).removeClass("simple-qa-active-vote");
				
					// current user already gave a vote
					if(simpleQAData.votes[v].users.indexOf(currentUserId) > -1) {
						if(charAfterString(simpleQAData.votes[v].users, currentUserId) == "+") {
							simpleQAData.votes[v].count = simpleQAData.votes[v].count - 1;
							simpleQAData.votes[v].users = simpleQAData.votes[v].users.replace(currentUserId + "+;", "");
						}
						else {
							simpleQAData.votes[v].count = simpleQAData.votes[v].count + 1;
							simpleQAData.votes[v].users = simpleQAData.votes[v].users.replace(currentUserId + "-;", "");
						}
					}
					
					// current user did not vote for that answer
					else {
					
						// vote up
						if(up) {
							simpleQAData.votes[v].count = simpleQAData.votes[v].count + 1;
							simpleQAData.votes[v].users = simpleQAData.votes[v].users + currentUserId + "+;";
							jz("#simple-qa-upvote-" + id).addClass("simple-qa-active-vote");
						}
						
						// vote down
						else {
							simpleQAData.votes[v].count = simpleQAData.votes[v].count - 1;
							simpleQAData.votes[v].users = simpleQAData.votes[v].users + currentUserId + "-;";
							jz("#simple-qa-downvote-" + id).addClass("simple-qa-active-vote");
						}
					}
	
					// remove the focus from the elements
					jz("#simple-qa-upvote-" + id).blur();
					jz("#simple-qa-downvote-" + id).blur();
					
					// update the counter
					jz("#simple-qa-vote-count-" + id).html(simpleQAData.votes[v].count);
					
					// post the changes
					postSimpleQAData(false);
				}
			}
		});
	};
	
	
	// approve an answer as the "right" one
	var approveAnswerInt = function(id) {
			
		var notify = false;
		
		// exist in case of anonymous user
		if(currentUserId == "") return;
		
		// if buttons are still blocked return - otherwise block them
		if(buttonsBlocked) return;
		blockButtons();
		
		// update data before continuing
		getSimpleQAData(function() {
			
			// remove all approved styles first (we add them later again if needed)
			jz('a').removeClass("simple-qa-approved");
			jz('span').removeClass("simple-qa-approved");
			jz('div').removeClass("simple-qa-approved-div");

			// is the currently approved answer clicked again?
			if(simpleQAData.approvedWithCommentId == id) {
				jz('#simple-qa-approve-' + id).blur();
				simpleQAData.approvedWithCommentId = "";			
			}
			
			// another answer is clicked
			else {
				jz('#simple-qa-approve-' + id).addClass("simple-qa-approved");
				jz("#simple-qa-div-" + id).addClass("simple-qa-approved-div");
				jz('#simple-qa-approve-' + id).blur();
				jz("#simple-qa-convert-" + id).hide();
				simpleQAData.approvedWithCommentId = id.toString();
				notify = true;
			}
			
			// update the data on the server
			postSimpleQAData(notify);
		});
	};
	
	
	// convert an answer to a comment
	var convertToCommentInt = function(id) {
		
		if(confirm(AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.convert.confirm"))) {
			
			// update data before continuing
			getSimpleQAData(function() {

				// exit early in case the answer is the approved one
				if(simpleQAData.approvedWithCommentId == id) { return; }
	
				// update UI
				displayAsComment(id);
				
				// change data
				for(var v = 0; v < simpleQAData.votes.length; v++) {
					if(simpleQAData.votes[v].id == id) {
						simpleQAData.votes[v].isComment = true;
						simpleQAData.votes[v].count = 9999;
					}
				}
				
				// update the data on the server
				postSimpleQAData(false);
			});
		}
	}
	
	
	// get the next character after searchFor
	var charAfterString = function(searchIn, searchFor) {
		var pos = searchIn.indexOf(searchFor);
		if(pos > -1) {
			return searchIn.substring(pos + searchFor.length, pos + searchFor.length + 1);
		}
		else {
			return "";
		}
	};
	
	
	// sort the answers
	var sortAnswers = function() {
		  jz(".simple-qa-answer").sort(sort_li).appendTo('#page-comments');
	};
	
	
	// internal function for sorting
	 var sort_li = function(a, b) {
		var diff = jz(b).prop('simple-qa-count') - jz(a).prop('simple-qa-count');
		if(diff == 0) {
			diff = jz(a).prop('simple-qa-commentId') - jz(b).prop('simple-qa-commentId');
		}
		return diff;
	};
	
	
	// start the mutation observer
	var startObserver = function() {
		var observe = mobile ? jz('.comment-list').get(0) : jz('#comments-section').get(0);
		commentAddedObserver.observe(observe, commentAddedObserverConfig);
	};
	
	
	// options for observing for added comments 
	var commentAddedObserverConfig = {
		childList : true,
		subtree : true
	};
	
	
	// the mutation observer for added comments
	var commentAddedObserver = new MutationObserver(function(mutations) {
		var reApply = false;
		mutations.forEach(function(mutation) {
			for(var n = 0; n < mutation.addedNodes.length; n++) {
				if(!mobile && (jz(mutation.addedNodes[n]).hasClass('comment-thread') || (jz(mutation.addedNodes[n]).find(".comment").length > 0))) {
					reApply = true;
				}
				if(mobile && jz(mutation.addedNodes[n]).hasClass('comment')) {
					reApply = true;
				}
			}
		});
		
		if(reApply && !mobile) {
			getSimpleQAData(function() {
				applyDecorators();			
			});
		}
		
		if(reApply && mobile) {
			getSimpleQAData(function() {
				applyDecoratorsMobile();		
			});
		}
	});
	
	
	// check for an orphaned data
	var checkForOrphanedData = function() {
		
		var updateNecessary = false;
		
		// check for orphaned approval
		if(simpleQAData.approvedWithCommentId != "") {
			if(jz("#comment-" + simpleQAData.approvedWithCommentId).length == 0) {
				simpleQAData.approvedWithCommentId = "";
				updateNecessary = true;
				console.log("Found orphaned approved answer - setting to not answered!");
			}
		}
		
		// check for orphaned answers
		while(checkForOrphanedAnswers()) {
			updateNecessary = true;
		}
		
		// was an update necessary => if yes post data to server
		if(updateNecessary) {
			postSimpleQAData(false);
		}
	};
	
	
	// check for orphaned vote entries
	var checkForOrphanedAnswers = function() {
		for(var n = 0; n < simpleQAData.votes.length; n++) {
			if(jz("#comment-" + simpleQAData.votes[n].id).length == 0) {
				console.log("Found orphaned answer: " + simpleQAData.votes[n].id);
				simpleQAData.votes.splice(n, 1);
				return true;
			}
		}
		return false;
	};
	
	
	// count the view
	var countView = function() {
		if("views" in simpleQAData) {
			simpleQAData.views = simpleQAData.views + 1;
		}
		else {
			simpleQAData.views = 1;
		}
		postSimpleQAData(false);
	};
	
	
	// get the Simple QA data from the server
	var getSimpleQAData = function(callback) {
		jz.ajax({
			url: baseUrl + "/plugins/servlet/simple-qa-data?pageId=" + pageId + "&userKey=" + currentUserId,
			type: "GET",
			contentType: "text/html",
			success: function(result) {
				var resultJSON = JSON.parse(result);
				simpleQAData = resultJSON.data;
				simpleQAConfig = resultJSON.config;
				lastUpdate = Date.now();
				callback();
			},
			error: function(request, status, error) {
				console.log("Error receiving Simple QA data from server!");
			}
		});
	};
	
	
	// set the Simple QA data on the server
	var postSimpleQAData = function(notify) {
		if(Date.now() - lastUpdate > 3000) {
			console.warn("Data not up-to-date. Skipping post.");
			return;
		}
		jz.ajax({
			url: baseUrl + "/plugins/servlet/simple-qa-data?pageId=" + pageId + "&notifyApproval=" + notify.toString(),
			type: "POST",
			contentType: "text/html",
			data : JSON.stringify(simpleQAData)
		});
	};
	
	
	// replace text which sounds like comments to more sound like answers
	var commentsToAnswers = function() {
		
		// replace the prompt for entering a comment, the header from Comments to Answers and the action button from reply to comment 
		jz(".quick-comment-prompt").html("<span>" + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.quick-comment-prompt.replace") + "</span>");
		jz(".action-reply-comment").children(":first-child").html("<span>" + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.comment-reply.replace") + "</span>");
		jz("#comments-section-title").html(AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.comments-section-title.replace"));
		
		// the "your answer" heading
		if(jz("#comments-section").find("#simple-qa-your-answer-heading").length == 0) {
			jz(".bottom-comment-panels").before("<div class='section-header' style='margin-top:13px;margin-bottom:-10px'><h4 id='simple-qa-your-answer-heading' class='section-title'>" +
				AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.yourAnswer") + "</h4></div>");
		}
		
		// hide like buttons
		jz(".comment-action-like").hide();
	};
	

	
	// replace text which sounds like comments to more sound like answers (mobile)
	var commentsToAnswersMobile = function() {
		jz(".comment-editor").attr("placeholder", AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.quick-comment-prompt.replace"));
		jz(".reply-button").text(AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.comment-reply.replace"));
	}

	
	// show an instructional message if requested
	var showMessage = function() {
		
		if(currentUserId == "") return;
		
		if(simpleQAConfig.showInstructions && !hasOptOut()) {
			require(['aui/flag'], function(flag) {
				messageFlag = flag({
					type: 'info',
		   			title: simpleQAConfig.instructionsTitle,
		   			body: simpleQAConfig.instructionsBody + "<p><button class='aui-button' type='button' onclick='simpleQA.optOut()'>" + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.simple-qa-overview.instructions.optOut") + "</button></p>"
				});
			});
		}
	};
	
	
	// handle an opt-out request for the instructional message
	var optOutInt = function() {
		try {
			messageFlag.close();
			localStorage.setItem("simple-qa-opt-out-" + AJS.Meta.get("parent-page-id") + "-" + currentUserId, "true");
		}
		catch(e) {}
	};
	
	
	// check for opt-out
	var hasOptOut = function() {
		try {
			return (localStorage.getItem("simple-qa-opt-out-" + AJS.Meta.get("parent-page-id") + "-" + currentUserId) == "true") ? true : false;
		}
		catch(e) {
			return false;
		}
	};
	
	
	// set buttonsBlocked for 500 ms
	var blockButtons = function() {
		buttonsBlocked = true;
		setTimeout(function() {
			buttonsBlocked = false;
		}, 500);
	}
}


// "class" to handle all Simple QA functions on the overview page
function SimpleQAOverview() {
	
	var activeFilter = "simple-qa-filter-all";
	var activeSearch = "";
	var pageSize = 10;
	var currentPage = 1;
	var numPages = 0;
	var activeSort = "4,1";
	
	
	// external functions
	this.createQuestion = function(ok) {
		createQuestionInt(ok);
	}
	
	this.createQuestionViaPrompt = function() {
		createQuestionViaPromptInt();
	}
	
	this.init = function() {
		var askButton = document.getElementById("simple-qa-overview-ask-button");
		if(askButton) {
			initFromUrl();
			getOverviewData(true);
			registerFilterCallback();
			askButton.addEventListener("click", function() {
				setTimeout(function() {
					AJS.$("#simple-qa-title").focus();
				}, 200);
			}, false);
		}
	}

	this.updateSearch = function() {
		updateSearchInt();
	}
	
	this.labelFilter = function(labelText) {
		labelFilterInt(labelText);
	}
	
	this.showPage = function(pageNum) {
		currentPage = pageNum;
		showCurrentPage();
	}

	this.nextPage = function() {
		nextPageInt();
	}

	this.prevPage = function() {
		prevPageInt();
	}

	
	// create a new question
	var createQuestionInt = function(ok) {
		document.getElementById('new-simple-qa-question-dialog').open = false;
		if(ok && AJS.$("#simple-qa-title").val() != "") {
			doCreateQuestion(AJS.$("#simple-qa-title").val());
			AJS.$("#simple-qa-title").val("");
		}
	}

	
	// create a new question via prompt (for older Confluence versions)
	var createQuestionViaPromptInt = function() {
		var title = prompt(AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.overview.create.title"));
		if(title != null) {
			doCreateQuestion(title);
		}
	}
	
	
	// create new questions
	var doCreateQuestion = function(title) {
		
		var templateId = AJS.$("#simple-qa-overview-ask-button").attr("templateId");
		var contentBlueprintId = AJS.$("#simple-qa-overview-ask-button").attr("contentBlueprintId");
		
		// template
		if((templateId != "default") && (templateId.length > 0)) {
			var data = {
				"spaceKey" : AJS.Meta.get("space-key"),
				"title" : title,
				"templateId" : parseInt(templateId)
			};
			Confluence.Blueprint.loadDialogAndOpenTemplate(data);
			return;
		}
		
		// blueprint
		if((contentBlueprintId != "default") && (contentBlueprintId.length > 0)) {
			var data = {
				"spaceKey" : AJS.Meta.get("space-key"),
				"title" : title,
				"contentBlueprintId" : contentBlueprintId
			};
			Confluence.Blueprint.loadDialogAndOpenTemplate(data);
			return;
		}
		
		// default (no template)
		title = encodeURIComponent(title);
		var bodyHint = encodeURIComponent("<p><span class='text-placeholder'>" + AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.create.bodyHint") + "</span></p><p class='simple-qa-hide'>" +
			"<img class='editor-inline-macro' src='" + AJS.contextPath() + "/download/resources/de.edrup.confluence.plugins.simple-qa/images/simple_qa_export_placeholder.png' data-macro-name='simple-qa-export-view' data-macro-schema-version='1'></p>");
		location.href = AJS.contextPath() + "/pages/createpage.action?spaceKey=" + AJS.Meta.get("space-key") +
			"&fromPageId=" + AJS.Meta.get("page-id") + "&title=" + title +
			"&wysiwygContent=" + bodyHint;
	}

	
	// register the filter change callback
	var registerFilterCallback = function() {
		if(AJS.$('#simple-qa-filter-tabs').length > 0) {
			AJS.$('#simple-qa-filter-tabs').on('tabSelect', function(e) {
				activeFilter = e.target.id;
				getOverviewData(false);
				updateUrl();
			});
		}
	}
	
	
	// set the search term
	var updateSearchInt = function() {
		activeSearch = AJS.$("#simple-qa-search-term").val();
		getOverviewData(false);
		updateUrl();
	}
	
	
	// filter by clicked label
	var labelFilterInt = function(labelText) {
		activeSearch = activeSearch.replace(/ *labelText:.*?( |$)/, "");
		if(activeSearch.length > 0) {
			activeSearch = activeSearch + " ";
		}
		activeSearch = activeSearch + "labelText:" + labelText;
		AJS.$("#simple-qa-search-term").val(activeSearch);
		getOverviewData(false);
		updateUrl();
	}
		
	
	// get the overview data from the server
	var getOverviewData = function(initial) {
		
		AJS.$("#simple-qa-load-spinner").spin();
		
		AJS.$.ajax({
			url: AJS.contextPath() + "/plugins/servlet/simple-qa-overview?pageId=" + AJS.Meta.get("page-id")
				+ "&userKey=" + AJS.Meta.get("remote-user-key") + "&filter=" + activeFilter + "&searchTerm=" + encodeURIComponent(activeSearch),
			type: "GET",
			contentType: "text/html",
			success: function(result) {
				AJS.$("#simple-qa-load-spinner").spinStop();
				AJS.$("#simple-qa-overview-table-section").html(result);
				AJS.$("#simple-qa-overview-table").sqatablesorter({
					sortList: [[parseInt(activeSort.substring(0,1)),parseInt(activeSort.substring(2,3))]],
					headers:{3:{sorter:false}}
				});
				AJS.$("#simple-qa-overview-table").bind("sortEnd", function() {
					initPagination(false);
					activeSort = AJS.$("#simple-qa-overview-table")[0].config.sortList.toString();
					updateUrl();
				});
				initPagination(initial);
			},
			error: function(request, status, error) {
				AJS.$("#simple-qa-load-spinner").spinStop();
				AJS.$("#simple-qa-overview-table").html(AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.overview.error.get"));
				initPagination(false);
			}
		});
	}
	
	
	// update the URL
	var updateUrl = function() {
		var currentUrl = window.location.href.replace(/[&\?]?simpleqafilter=.*/, "");
		currentUrl = (currentUrl.indexOf("?") > 0) ? currentUrl + "&" : currentUrl + "?";
		currentUrl = currentUrl + "simpleqafilter=" + activeFilter + "&simpleqasearch=" + encodeURIComponent(activeSearch) + 
			"&simpleqasort=" + encodeURIComponent(activeSort) +  "&simpleqapage=" + currentPage;
		history.pushState(null, null, currentUrl);
	}
	
	
	// init from the URL
	var initFromUrl = function() {
		activeFilter = getParameterByNameWithDefault("simpleqafilter", window.location.href, "simple-qa-filter-all");
		activeSearch = getParameterByNameWithDefault("simpleqasearch", window.location.href, "");
		activeSort = getParameterByNameWithDefault("simpleqasort", window.location.href, "4,1");
		currentPage = parseInt(getParameterByNameWithDefault("simpleqapage", window.location.href, "1"));
		
		AJS.$("#simple-qa-search-term").val(activeSearch);
		AJS.tabs.change(AJS.$("#" + activeFilter));
	}
	
	
	// get an Url parameter by its name
	var getParameterByNameWithDefault = function(name, url, defaultParam) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return defaultParam;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	
		
	// init pagination
	var initPagination = function(initial) {
		
		// determine the number of pages
		numPages = Math.ceil(AJS.$("#simple-qa-overview-table").find(".simple-qa-content-tr").length / pageSize);
		
		// in case we have at least one page
		if(numPages > 0) {
			if(!initial || (currentPage > numPages)) {
				currentPage = 1;
			}
			initPaginationControls();
			showCurrentPage();
		}
		
		// if there is no page don't show any pagination controls
		else {
			AJS.$("#simple-qa-pagination-section").html("");
		}
	}
	
	
	// build the pagination controls
	var initPaginationControls = function() {
		
		// previous control
		var navLinks = "<span style='padding-right:3px'><a class='simple-qa-pagination-link' id='simple-qa-pagination-link-prev' href='javascript:simpleQAOverview.prevPage();'>" +
			AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.overview.table.previous") + "</a></span>";
		
		// the links to the pages
		for(var n = 0; n < numPages; n++) {
			var navLink = "<span style='padding-left:3px;padding-right:3px'><a class='simple-qa-pagination-link' id='simple-qa-pagination-link-pageNum' href='javascript:simpleQAOverview.showPage(pageNum);'>pageNum</a></span>";
			navLink = navLink.replace(/pageNum/g, n + 1);
			navLinks = navLinks + navLink;
		}
		
		// next control
		navLinks = navLinks + "<span style='padding-left:3px'><a class='simple-qa-pagination-link' id='simple-qa-pagination-link-next' href='javascript:simpleQAOverview.nextPage();'>" +
			AJS.I18n.getText("de.edrup.confluence.plugins.simple-qa.overview.table.next") + "</a></span>";
		
		// set it
		AJS.$("#simple-qa-pagination-section").html(navLinks);
	}
	
	
	// show the current page
	var showCurrentPage = function() {
		
		// hide all elements first
		AJS.$("#simple-qa-overview-table").find(".simple-qa-content-tr").hide();

		// show the elements of our the current page
		var elements = AJS.$("#simple-qa-overview-table").find(".simple-qa-content-tr");
		var stopBefore = ((pageSize * currentPage) > elements.length) ? elements.length : (pageSize * currentPage);
		for(n = pageSize * (currentPage - 1); n < stopBefore; n++) {
			AJS.$(elements[n]).show();
		}

		// disable the link to the current page and enable all other ones
		AJS.$("#simple-qa-pagination-section").find(".simple-qa-pagination-link").removeClass("simple-qa-pagination-link-inactive");
		AJS.$("#simple-qa-pagination-section").find(".simple-qa-pagination-link").removeClass("simple-qa-pagination-link-current");
		AJS.$("#simple-qa-pagination-link-" + currentPage).addClass("simple-qa-pagination-link-current");

		// disable prev and next link depending on the page we are on
		if(currentPage == 1) {
			AJS.$("#simple-qa-pagination-link-prev").addClass("simple-qa-pagination-link-inactive");
		}
		if(currentPage == numPages) {
			AJS.$("#simple-qa-pagination-link-next").addClass("simple-qa-pagination-link-inactive");
		}
		
		// update the URL
		updateUrl();
	}

	
	// show the next page
	var nextPageInt= function() {
		if(currentPage < numPages) {
			currentPage = currentPage + 1;
			showCurrentPage();
		}
	}

	
	// show the previous page
	var prevPageInt= function() {
		if(currentPage > 1) {
			currentPage = currentPage - 1;
			showCurrentPage();
		}
	}
}


var simpleQA = new SimpleQA();
var simpleQAOverview = new SimpleQAOverview();


// start Simple QA on page load
AJS.toInit(function(){
	simpleQA.init(false, AJS.$);	
	simpleQAOverview.init();
});