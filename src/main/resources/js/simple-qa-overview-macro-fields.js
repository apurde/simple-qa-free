AJS.toInit(function ($) {

    function getWebItemsSync(callback) {
        Confluence.Blueprint.Dialog.requestWebItems(AJS.Meta.get("space-key"), false).done(function (ev) {
        	var defaultConfig = {
        		"name" : "Simple QA default",
        		"templateId" : "default",
        		"blueprintModuleCompleteKey" : "default",
        		"contentBlueprintId" : "default"
        	};
            var configs = Confluence.Blueprint.Dialog.loadedWebitems[AJS.Meta.get("space-key")];
            configs.unshift(defaultConfig);
            callback(ev, configs);

        }).fail(function () {
			console.log("Error collecting templates!");
        });
    }

    function fillField(input) {
        getWebItemsSync(function(ev, configs) {
            var selected = input.val();
            input.empty();
            _.each(configs, function(config) {
                var itemModuleCompleteKey = config.itemModuleCompleteKey;
                if ((itemModuleCompleteKey == "com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page"
                        || itemModuleCompleteKey == "com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blog-post"))
                    return;
                if (!(config.templateId || config.contentBlueprintId))
                    return;
                var option = $('<option></option>').text(config.name);
                option.attr("data-template-id", config.templateId);
                option.attr("data-blueprint-module-complete-key", config.blueprintModuleCompleteKey);
                option.attr("data-content-blueprint-id", config.contentBlueprintId);
                //option.attr("data-create-result", config.createResult);
                option.val(config.templateId || config.contentBlueprintId);
                input.append(option);
            });
            input.val(selected);
        });
    }

    var overrides = {
  
        beforeParamsSet: function beforeParamSetOverride(selectedParams, selectedMacro) {
            fillField($("#macro-param-templateName"));
            return selectedParams;
        },

        beforeParamsRetrieved: function beforeParamsRetrievedOverride(paramMap, macro, sharedParamMap) {
            var option = AJS.MacroBrowser.fields['templateName'].input.find("option:selected");
            paramMap["blueprintModuleCompleteKey"] = option.data("blueprint-module-complete-key");
            paramMap["contentBlueprintId"] = option.data("content-blueprint-id");
            paramMap["templateId"] = option.data("template-id");
            //paramMap["createResult"] = option.data("create-result");
            return paramMap;
        }
    };

    AJS.MacroBrowser.setMacroJsOverride("simple-qa-overview", overrides);
});
