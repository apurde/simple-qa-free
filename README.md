# Simple QA add-in
The Simple QA add-in brings basic question and answer functions to Confluence by extending the page/comment concept. 

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer to the Wiki pages of this repository.

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the master branch.

## Building

Run the following commands on the sources (you need Atlassian's SDK for that):

```
atlas-clean
atlas-package
```  
